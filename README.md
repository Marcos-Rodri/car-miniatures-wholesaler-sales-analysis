# Car Miniatures Wholesaler - Sales Analysis

In this study, we are going to develop a Sales Analytics project by using the 'Classic Models Sample' database, which contains information on a wholesale company that sells miniatures of cars, motorcicles, ships, trains, among other products. This firm's customers are retailers that acquire miniatures to resell them to the end user.


The objectives of this study are the following: 

- Generate insights that will enable the wholesaler's directors to make assertive decisions;
- Keep track of the firm's KPIs and register them for future use.

How we are going to achieve that:

- By using SQL queries to manipulate and select the data that we want to analyze;
- By creating visualizations with Tableau, so we can interpret the information;

Questions:

- 1) To which countries the company sells more in terms of revenue?


- 2) What was the company' revenue in each year?


- 3) Which were the best-selling product lines in 2003 and 2004?


- 4) Which were the product lines that generated the highest revenues in 2003 and 2004?


- 5) What was the average price for each product line in 2003 and 2004?


- 6) If the company wanted to give a reward to the best employee of each year, how would you choose them?


- 7) The HR department needs to access information about the firm's employees everyday. How can you automate this process?


- 8)  Which customers acquired at least 80.000 dollars in products in 2004?


- 9)  Are there any cases where the total payments made by customers don't match the total value of their orders?


Data Source: The "classicmodels" database is provided by MySQLtutorial website and can be found at: https://www.mysqltutorial.org/mysql-sample-database.aspx/
